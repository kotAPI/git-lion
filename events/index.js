import {
    dialog,
    ipcMain
} from 'electron';

import repos from "./repos/index"

const Events = {
    initialize: () => {
        repos(ipcMain)
    }
}

export default Events;