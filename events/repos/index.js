import {
    dialog
} from 'electron';

import Repos from "../../db/collections/repos/index"

const repos = (ipcMain) => {
    ipcMain.on('hey-open-my-dialog-now', async () => {
        let res = await dialog.showOpenDialog({ properties: ['openDirectory', 'createDirectory'], message: "Pick a repository you want to add to Git Lion" })
        // create new repo here
       Repos.addNewRepo({path:res?.filePaths[0],_id:"res"})

    });
}

export default repos;