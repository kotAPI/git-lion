const fs = require('fs')
const http = require("isomorphic-git/http/node");

import { currentBranch, fetch, push } from 'isomorphic-git'
import _ from "lodash"
import { ipcRenderer } from 'electron';

export const getCurrentBranch = async (localGitFolderPath) => {
  return await currentBranch({
    fs,
    dir: localGitFolderPath,
    fullname: false
  })
}

export const fetchBranches = async (localGitFolderPath) => {

  return await fetch({
    fs,
    http,
    dir: localGitFolderPath
  })

}

export const pushBranch =  async (localGitFolderPath) => {

  return await push({
    fs,
    http,
    dir: localGitFolderPath,
    remote: 'origin',
    onAuthFailure: (url, auth) => {
     console.log(url, auth)
      alert("failed")
    },
    onAuth: () => ({ username:"@kotAPI", password: 'glpat-axcAssgoVQzJvzrN4Fbs' }),
  })

}