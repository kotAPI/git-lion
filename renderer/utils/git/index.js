// node.js example
const path = require('path')
const http = require('isomorphic-git/http/node')
const fs = require('fs')

import { plugins, clone, commit, push, findRoot,log, listBranches,listFiles } from 'isomorphic-git'

const dir = path.join(process.cwd(), 'test-clone')
const root = path.join(process.cwd())


const GIT_URL = 'https://github.com/kotAPI/git-lion'

export { fetchUnstaged, getChangedFiles } from './files'
export {getCurrentBranch} from "./branches"


export const CloneRepo = () => {
    clone({ fs, http, dir, url: GIT_URL }).then(console.log)
}

export const fetchCommits = async (localGitFolderPath) => {
    let commits = await log({
        fs,
        dir: localGitFolderPath,
        depth:50
    })
    return commits
}

export const folderIsAGitRepo = async (localGitFolderPath) => {
    let fullPath = localGitFolderPath
    return await findRoot({
        fs,
        filepath: fullPath
    })
}

export const getBranches= async(localGitFolderPath, getRemote=false)=>{
    let query = {
        fs,
        dir:localGitFolderPath
    }
    if(getRemote){
        query.remote = "origin"
    }
    
    let remoteBranches = await listBranches(query)
    return remoteBranches
} 
