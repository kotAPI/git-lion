const fs = require('fs')
import { listFiles, log, walk, statusMatrix, readTree, TREE, add, commit } from 'isomorphic-git'
import _ from "lodash"
import { ipcRenderer } from 'electron';

export const addFile = async (localGitFolderPath, filepath) => {
    try {
        return await add({ fs, dir: localGitFolderPath, filepath: filepath })
    } catch (err) {
        console.log(err)
    }
}

export const removeFile = async (localGitFolderPath, filepath) => {
    try {
        //return await remove({ fs, dir:localGitFolderPath,filepath:filepath })

    } catch (err) {
        console.log(err)
    }
}

export const fetchUnstaged = async (localGitFolderPath) => {
    try {
        const FILE = 0, HEAD = 1, WORKDIR = 2, STAGE = 3;

        // https://isomorphic-git.org/docs/en/statusMatrix#q-what-files-will-not-be-changed-if-i-commit-right-now
        const statusMapping = {
            "003": "added, staged, deleted unstaged",
            "020": "new, untracked",
            "022": "added, staged",
            "023": "added, staged, with unstaged changes",
            "100": "deleted, staged",
            "101": "deleted, unstaged",
            "103": "modified, staged, deleted unstaged",
            "111": "unmodified",
            "121": "modified, unstaged",
            "122": "modified, staged",
            "123": "modified, staged, with unstaged changes"
        };

        let matrixResults = (await statusMatrix({ fs, dir: localGitFolderPath }))
            .filter(row => row[HEAD] !== row[WORKDIR] || row[HEAD] !== row[STAGE]);

        let allUncommitedChanges = matrixResults.map(row => statusMapping[row.slice(1).join("")] + ": " + row[FILE]);

        let finalFiles = _.map(allUncommitedChanges, (item) => {
            let fileObj = {}
            let arr = item.split(":")
            let statuses = arr[0].split(",")
            statuses = statuses.map(status => { return status.trim() })

            fileObj.state = statuses
            fileObj.path = arr[1].trim()
            return fileObj
        })

        return finalFiles

        


    } catch (err) {
        console.log(err)
        throw new Error(err)
    }
}

export const Commit = async({localGitFolderPath, gitMessage})=>{
    try{
        let sha = await commit({
            fs,
            dir: localGitFolderPath,
            author: {
              name: 'Pranay Kothapalli',
              email: 'pranaykothapalli@gmail.com',
            },
            message: gitMessage
          })
          console.log(sha)
    
        return sha
    }catch(err){
        console.log(err)
    }
}

export const getChangedFiles = async (localGitFolderPath, commit) => {
    try {
        const FILE = 0, HEAD = 1, WORKDIR = 2

        const filenames = (await statusMatrix({ fs, dir: localGitFolderPath, ref:commit.oid }))
            .filter(row => row[HEAD] !== row[WORKDIR])
            .map(row => row[FILE])

        return filenames
    } catch (err) {
        console.log(err)
    }
}