import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { PageLayout } from "../../components/ui/index"
import _ from "lodash"
import Home from '../../components/functional/Home/Home';
import NavBar from '../../components/functional/NavBar/index'

import RepoView from '../../components/functional/RepoView/RepoView';


const DEFAULT_TAB = {
  label: "HOME",
  tab_data: {}
}

function HomePage() {
  const [tabs, setTabs] = useState([])
  const [currentTab, setCurrentTab] = useState(DEFAULT_TAB)

  const openTab = (tab) => {
    // Check if tab already exists, exit early
    let filteredTabsCheck = _.filter(tabs, (tabItem) => {
      return tabItem.label === tab.label
    })

    if (filteredTabsCheck.length) {
      setCurrentTab(tab)
      return
    }
    let newTabs = tabs.concat(tab)
    setCurrentTab(tab)
    setTabs(newTabs)
  }

  useEffect(() => {
    if (!tabs.length) {
      setTabs([DEFAULT_TAB])
    }
  }, [currentTab, tabs])

  return (
    <React.Fragment>
      <Head>
        <title>Git Lion</title>
      </Head>
      {/* Page start  */}
      <div className='overflow-y-hidden h-screen'>
        <NavBar currentTab={currentTab} openTab={openTab} tabs={tabs} />
        <PageLayout>
          {currentTab.label === "HOME" ? <Home currentTab={currentTab} openTab={openTab} /> : null}
          {currentTab.label === "REPO" ? <RepoView currentTab={currentTab} openTab={openTab} /> : null}
        </PageLayout>
      </div>
      {/* Page End */}
    </React.Fragment>
  );
}

export default HomePage;
