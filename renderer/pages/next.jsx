import React from 'react';
import Head from 'next/head';
import Link from 'next/link';

import {CloneRepo} from "../utils/git/index"

function Next() {
  const cloneRepoHandler = async()=>{

  }
  return (
    <React.Fragment>
      <Head>
        <title>Git Lion - Page</title>
      </Head>
      <div className='grid grid-col-1 text-2xl w-full text-center'>
        
        <span className='py-10 my-10'> Next Page yo </span>
      </div>
      <div className='mt-1 w-full flex-wrap flex justify-center'>
        <Link href='/home'>
          <a className='btn-blue'>Go to home page</a>
        </Link>
        <button onClick={cloneRepoHandler}>Clone</button>
      </div>
    </React.Fragment>
  )
}

export default Next;
