import React from 'react';
import '../styles/globals.css';

import DB from "../../db/index"

function MyApp({ Component, pageProps }) {
  DB.init()
  return <Component {...pageProps} />;
}

export default MyApp;
