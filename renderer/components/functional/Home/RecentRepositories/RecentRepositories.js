import { PageLayout, Typography, Button } from "../../../ui/index"
import React, { useEffect, useState } from 'react';
import { locateFolder } from "../../../../utils/fileLocator/index"
import  Repos  from "../../../../../db/collections/repos/index"

import RecentRepoItem from "./RecentRepoItem/RecentRepoItem";

const RecentRepositories = ({openTab}) => {
    const [repos,setRepos] = useState([])

  const fetchRecentRepos = async() => {
    let res =  await Repos.fetchRecent()
    setRepos(res)
   }
 
 
   useEffect(() => {
     fetchRecentRepos()
   }, [])


    const addRepo = async () => {
        await locateFolder()
        setTimeout(()=>{
            fetchRecentRepos()
        },6000)
    }


    return <div>
        <Typography.H1>Recent Repositories</Typography.H1>
        <div>
            <div>
                <ul>
                    {Object.keys(repos).length && repos.map((repo,idx)=>{
                        return <RecentRepoItem setRepos={setRepos} key={idx} repo={repo} openTab={openTab}>{repo.path}</RecentRepoItem>
                    })} 
                </ul>
            </div>
            <div>
                <Button onClick={addRepo}>Add Repo</Button>
            </div>
        </div>
    </div>
}

export default RecentRepositories