import Repos from "../../../../../../db/collections/repos/index"

const RecentRepoItem = ({ children, repo, openTab,setRepos }) => {

    const openRepoTab = () => {
        openTab({
            label: "REPO",
            tab_data: repo
        })
    }

    const removeFromRecents = async() => {
       let updatedRepos = await Repos.deleteRecentRepo(repo)
       await setRepos(updatedRepos)
    }

    return <div className="flex items-center" >
        <div onClick={openRepoTab}>{children}</div>
        <div className="ml-4 text-red-600 font-bold cursor-pointer" onClick={removeFromRecents}>X</div>
    </div>
}

export default RecentRepoItem