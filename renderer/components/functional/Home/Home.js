import RecentRepositories from "./RecentRepositories/RecentRepositories"

const Home = ({ openTab }) => {
    return <div>
        <RecentRepositories openTab={openTab} />
    </div>
}
export default Home