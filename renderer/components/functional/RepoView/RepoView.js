import {useEffect, useState} from "react"

import { folderIsAGitRepo } from "../../../utils/git"

import MainRepoView  from "./MainRepoView/MainRepoView"

const RepoView = ({currentTab})=>{

    const [repoData,setRepoData] = useState(currentTab.tab_data)
    const [repoState,setRepoState] = useState("LOADING")

    const checkIfGitRepo = async()=>{
        try{
            let res = await folderIsAGitRepo(repoData?.path)
            setRepoData(currentTab.tab_data)
            setRepoState("TRUE")
            
        }catch(err){
            console.log(err)
            setRepoState("NOT_A_GIT_REPO")
        }
    }

    useEffect(()=>{

       checkIfGitRepo();

    },[repoState,repoData])




    return <div>
       {repoState==="LOADING" && <div> LOADING RA</div>}
       {repoState==="NOT_A_GIT_REPO" && <div> NOT_A_GIT_REPO RA</div>}
       {repoState==="TRUE" && <MainRepoView repoData={repoData} />}
    </div>
}




export default RepoView