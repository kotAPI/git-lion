import { useState, useEffect } from "react"
import { fetchCommits, fetchUnstaged, getCurrentBranch } from "../../../../utils/git/index"

import { Typography } from "../../../ui/index"
import CommitPane from "./CommitPane/CommitPane"
import BranchPane from "./BranchPane/BranchPane"
import CommitDetailPane from "./CommitDetailPane/CommitDetailPane"
import RepoControls from "./RepoControls/RepoControls"


const MainRepoView = ({ repoData }) => {
    const [commits, setCommits] = useState([])
    const [currentCommit, setCurrentCommit] = useState()
    const [selectedCommit, setSelectedCommit] = useState()

    const [viewingStaged, setViewingStaged] = useState(false)
    const [changesAvailable, setChangedAvailable] = useState([])

    //
    const [currentBranch, setCurrentBranch] = useState()


    const fetchCurrentBranch = async()=>{
        try{
            let res = await getCurrentBranch(repoData.path);
            setCurrentBranch(res)
        }catch(err){

        }
    }


    const getUnstagedFiles = async (commit) => {
        try {
            let fileRes = await fetchUnstaged(repoData.path, commit.oid)
            //
            let files = fileRes.filter(file => {
                return file.state.includes("unstaged")
            })
            if (files.length) {
                setChangedAvailable(files)
            }


        } catch (err) {
            console.log(err)
        }
    }

    //
    const initCommits = async () => {

        try {
            let res = await fetchCommits(repoData.path)
            fetchCurrentBranch()
            setCommits(res)
            setCurrentCommit(res[0])
            setSelectedCommit(res[0])
            await getUnstagedFiles(res[0])


        }
        catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        initCommits()
    }, [])

    return <div>

        <div >
            <RepoControls repoData={repoData} />
            <div className="flex">
                <BranchPane currentBranch={currentBranch} repoData={repoData} />
                <CommitPane changesAvailable={changesAvailable} viewingStaged={viewingStaged} setViewingStaged={setViewingStaged} setSelectedCommit={setSelectedCommit} selectedCommit={selectedCommit} currentCommit={currentCommit} repoData={repoData} commits={commits} />
                {(selectedCommit) && <CommitDetailPane refreshData={initCommits} viewingStaged={viewingStaged} currentCommit={currentCommit} selectedCommit={selectedCommit} repoData={repoData} />}
            </div>
        </div>
    </div>
}

export default MainRepoView