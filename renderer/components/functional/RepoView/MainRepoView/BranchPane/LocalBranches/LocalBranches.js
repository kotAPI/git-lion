import { DownOutlined } from '@ant-design/icons';
import { Tree, Popover, Modal } from 'antd';
import _ from 'lodash';
import { useState } from 'react';


const LocalBranches = ({ branches, title, currentBranch }) => {
    const [open, setOpen] = useState(false)

    let treeDb = _.map(branches, (branch, idx) => {
        let activeClasses = ''

        if (branch === currentBranch) {
            activeClasses = 'bg-green-500 text-white'
        }

        return {
            title: <Popover placement="right" className='text-xs' title={branch}>
                <div onClick={() => { setOpen(true) }} className={`truncate px-2 py-1 text-xs h-auto flex items-center ${activeClasses}`} style={{ width: "160px" }}>{branch}</div>
            </Popover>,
            key: idx,
            children: []
        }
    })


    return <div className="p-2 mb-1">
        <div className="font-bold text-xs">
            {title} ({branches.length})
        </div>

        <ul className="overflow-y-scroll overflow-x-hidden py-2 bg-white mt-1 pr-1" style={{ height: '40vh' }}>
            <Tree
                switcherIcon={<DownOutlined />}
                defaultExpandedKeys={['0-0-0']}
                treeData={treeDb}
            />
        </ul>
        <Modal
            title="Modal 1000px width"
            centered
            open={open}
            onOk={() => setOpen(false)}
            onCancel={() => setOpen(false)}
            width={1000}
        >
            <p>some contents...</p>
            <p>some contents...</p>
            <p>some contents...</p>
        </Modal>
    </div>
}

export default LocalBranches