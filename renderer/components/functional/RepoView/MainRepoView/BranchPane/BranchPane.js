import { useEffect, useState } from "react"
import { getBranches } from "../../../../../utils/git"
import LocalBranches from "./LocalBranches/LocalBranches"

const BranchPane = ({repoData,currentBranch})=>{

    const [localBranches,setLocalBranches] = useState([])
    const [remoteBranches,setRemoteBranches] = useState([])

    const fetchBranches = async()=>{
        try{
           let branches =  await getBranches(repoData.path,false)
           let remoteBrches =  await getBranches(repoData.path,true)

           setLocalBranches(branches)
           setRemoteBranches(remoteBrches)
        }catch(err){
            console.log(err)
        }
    }

    useEffect(()=>{
        //
        fetchBranches()
    },[])

    return <div className="flex-none bg-blue-100" style={{width:"200px"}}>
        <LocalBranches currentBranch={currentBranch} title="Local branches" branches={localBranches}/>
        <LocalBranches  title="Remote branches" branches={remoteBranches}/>
    </div>
}

export default BranchPane