import CommitListItem from "./CommitListItem/CommitListItem"

const CommitPane = ({ commits, repoData, currentCommit, setSelectedCommit, selectedCommit, setViewingStaged, changesAvailable }) => {

    const viewStagedFiles = () => {
        setViewingStaged(true)
    }
    // 
    return <div className="flex-1 h-screen pb-20 overflow-y-scroll">
        {changesAvailable && <div className="text-xs bg-blue-700 text-white p-2 flex items-center justify-between">
            <div>{changesAvailable.length} Untracked files</div>
            <div>
                <button className="border border-white text-white background-transparent p-1 px-4 hover:bg-white hover:text-black" onClick={viewStagedFiles}>View Files</button>
            </div>
        </div>}
        {/*  */}
        {commits && commits.map((commitItem, idx) => {
            return <CommitListItem setViewingStaged={setViewingStaged} setSelectedCommit={setSelectedCommit} selectedCommit={selectedCommit} currentCommit={currentCommit} commit={commitItem} key={idx} />

        })}
    </div>
}

export default CommitPane