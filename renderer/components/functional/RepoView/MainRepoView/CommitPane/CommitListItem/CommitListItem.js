import { Avatar, Image } from 'antd';
import { UserOutlined } from '@ant-design/icons';

import { timeAgo } from '../../../../../../utils/date';

const CommitListItem = ({ commit, currentCommit, setSelectedCommit, selectedCommit, setViewingStaged }) => {


  let isCurrent = currentCommit?.oid === commit?.oid;
  let isSelected = commit?.oid === selectedCommit?.oid


  let activeClassess = isCurrent ? 'bg-green-200 hover:bg-green-200' : 'hover:bg-gray-100'
  let selectedClasses = isSelected ? 'bg-blue-200 hover:bg-blue-300' : ''


  const selectCommit = () => {
    // had done this as a hack to re render ui as parent depends on it
    setSelectedCommit(undefined)
    setViewingStaged(false)
    setTimeout(() => {
      setSelectedCommit(commit)
    }, 1)
  }

  console.log(commit)

  return <div className={`${activeClassess} ${selectedClasses} overflow-hidden flex items-center cursor-pointer pl-4 py-1 w-full`} onClick={selectCommit}>
    <div className="flex items-center w-full justify-between">
      {/*  */}
      <div className="flex items-center w-full " style={{minWidth:"400px"}}>
      <Avatar icon={<UserOutlined />} shape="square" size={24} className="mr-2 "></Avatar>
        {/* <div className="font-bold mr-2 text-xs truncate" >{commit.commit.author.name}</div> */}
        <div className="truncate text-xs" style={{width:"50%"}}>{commit.commit.message}</div>
      </div>
      {/*  */}
      <div style={{width:"120px"}} className="text-right pr-2 text-xs text-gray-700">
        {timeAgo(commit.commit.author.timestamp)}
      </div>
    </div>
  </div>
}

export default CommitListItem