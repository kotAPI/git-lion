

import { addFile, removeFile } from "../../../../../../utils/git/files"

const FileItem = ({ file, refreshFiles, repoData }) => {

    // 
    const stageFile = async () => {
        await addFile(repoData.path, file.path)
        refreshFiles()
    }

    const unstageFile = async ()=>{
        await removeFile(repoData.path, file.path)
        refreshFiles()

        
    }

    const ActionsRenderer = () => {
        let isUnstaged = file.state.includes("unstaged") || file.state.includes("untracked")
        let isStaged =file.state.includes("staged")
        return <>
           {isUnstaged && <span className="ml-2" onClick={stageFile}>Stage</span>} 
           {isStaged && <span className="ml-2" onClick={unstageFile}>Unstage</span>} 
        </>
    }

    return <>
        <li className="flex items-center">{file.path.slice(0,20)} <ActionsRenderer /> </li>
    </>
}

export default FileItem