import { useEffect, useState } from "react"

import { fetchUnstaged, getChangedFiles } from "../../../../../../../utils/git"
import FileItem from "../../FileItem/FileItem"
import _ from "lodash"

import CommitBox from "./CommitBox/CommitBox"

const StagingArea = ({ repoData,selectedCommit, fetchFilesToDisplay, refreshData }) => {
    const [untrackedFiles, setUntrackedFiles] = useState([])
    const [stagedFiles, setStagedFiles] = useState([])

    const getUnstagedFiles = async () => {
        try {
            let fileRes = await fetchUnstaged(repoData.path, selectedCommit.oid)
            //
            setUntrackedFiles(fileRes.filter(file => {
                return file.state.includes("unstaged") || file.state.includes("untracked")
            }))
            //
            setStagedFiles(fileRes.filter(file => {
                return file.state.includes("staged")
            }))
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getUnstagedFiles()
    }, [])


    return <div>

        Files
        <ul>
            <h1 className="font-bold">Untracked</h1>
            {untrackedFiles.map((file, idx) => {
                return <FileItem repoData={repoData} refreshFiles={getUnstagedFiles} key={idx} file={file} />
            })}
        </ul>
        <ul>
            <h1 className="font-bold">Staged</h1>
            {stagedFiles.map((file, idx) => {
                return <FileItem repoData={repoData} refreshFiles={getUnstagedFiles} key={idx} file={file} />
            })}
        </ul>
        {/*  */}
        <CommitBox repoData={repoData} refreshData={refreshData} getUnstagedFiles={getUnstagedFiles}  />
    </div>
}

export default StagingArea