import { useState } from "react"

import { Commit } from "../../../../../../../../utils/git/files"

const CommitBox = ({ repoData, refreshData, getUnstagedFiles }) => {
    const [commitMessage, setCommitMessage] = useState('')

    const commit = async () => {
        try {
            console.log(commitMessage)
            let res = await Commit({ localGitFolderPath: repoData.path, gitMessage: commitMessage })
            console.log(res)
            getUnstagedFiles()
            await refreshData()
            this.setCommitMessage("")

        } catch (err) {
            console.log(err)
        }
    }

    return <div className="border border-blue-300 bg-white p-3">
        <input value={commitMessage} onChange={(e) => { setCommitMessage(e.target.value) }} className="border border-gray-500 focus:outline-none px-1 w-full" type="text" />
        <textarea className="border border-gray-500 focus:outline-none px-1 w-full mt-4" type="text" />

        <div>
            <button className="bg-green-500 text-white px-2 py-1 hover:bg-green-600" onClick={commit}>Commit</button>
        </div>
    </div>
}

export default CommitBox