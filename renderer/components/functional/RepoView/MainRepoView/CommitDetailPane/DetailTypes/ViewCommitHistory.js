import { useEffect, useState } from "react"
import { getChangedFiles } from "../../../../../../utils/git"
import { Tree, Popover, Modal } from 'antd';

const ViewCommitHistory = ({repoData,selectedCommit})=>{

    const [files,setFiles] = useState([])

    useEffect(()=>{
        getChangedFilesBetweenCommits()
    },[])

    const getChangedFilesBetweenCommits = async () => {
        let res = await getChangedFiles(repoData.path,selectedCommit)
        setFiles(res)
    }



    return <>
        <ul>
            {files.map((file,idx)=>{
                return <Popover key={idx} placement="left" title={file}>
                    <li className="truncate text-xs bg-gray-300 mb-1 px-2 py-1 cursor-pointer" style={{width:"98%"}} >{file}</li>
                </Popover> 
            })} 
        </ul>
    </>
}

export default ViewCommitHistory