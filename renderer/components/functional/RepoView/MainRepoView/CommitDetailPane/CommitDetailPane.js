import { useEffect, useState } from "react"
import { fetchUnstaged, getChangedFiles } from "../../../../../utils/git"
import FileItem from "./FileItem/FileItem"
import _ from "lodash"


import ViewCommitHistory from "./DetailTypes/ViewCommitHistory"
import StagingArea from "./DetailTypes/StagingArea/StagingArea"

const CommitDetailPane = ({ selectedCommit, currentCommit, repoData, viewingStaged, refreshData }) => {
    const [untrackedFiles, setUntrackedFiles] = useState([])
    const [stagedFiles, setStagedFiles] = useState([])

    const getUnstagedFiles = async () => {
        try {
            let fileRes = await fetchUnstaged(repoData.path, selectedCommit.oid)
            //
            setUntrackedFiles(fileRes.filter(file=>{

                return file.state.includes("unstaged")
            }))
            //
            setStagedFiles(fileRes.filter(file=>{

                return file.state.includes("staged")
            }))
        } catch (err) {
            console.log(err)
        }
    }
    //
    const getChangedFilesBetweenCommits = async () => {
        let res = await getChangedFiles(repoData.path,selectedCommit)

    }

    const fetchFilesToDisplay = async() => {
       
        if (selectedCommit?.oid === currentCommit?.oid) {
            await getUnstagedFiles()
        } else {
            await getChangedFilesBetweenCommits()
        }

    }

    useEffect(() => {
        fetchFilesToDisplay()
    }, [])

    return <div style={{ width: "350px",height:"calc(100vh - 50px)" }} className="flex-none bg-blue-100 p-2 overflow-y-scroll overflow-x-hidden">
        {!viewingStaged && <ViewCommitHistory selectedCommit={selectedCommit} repoData={repoData} />}
        {viewingStaged&& <StagingArea refreshData={refreshData} fetchFilesToDisplay={fetchFilesToDisplay} selectedCommit={selectedCommit} repoData={repoData} />}
    </div> 
    
}

export default CommitDetailPane