import { fetchBranches,pushBranch } from "../../../../../utils/git/branches"

const RepoControls = ({repoData}) => {


    const fetchFromOrigin = async()=>{
        try{
            console.log("fetching")

            let res = await fetchBranches(repoData.path)
            console.log(res)

        }catch(err){
            console.log(err)
        }
    }

    const push = async()=>{
        try{
            console.log("pushing")
            let res = await pushBranch(repoData.path)
            console.log(res)

        }catch(err){
            console.log(err)
        }
    }

    return <div className="bg-gray-200 text-xs">
        <div className="flex items-center justify-center">
            <div>
                <ul className="flex items-center">
                    <li className="px-2 cursor-pointer hover:bg-gray-500 hover:text-white p-2" onClick={push}>↑ Push</li>
                    <li className="px-2 cursor-pointer hover:bg-gray-500 hover:text-white p-2">↓ Pull</li>
                    <li className="px-2 cursor-pointer hover:bg-gray-500 hover:text-white p-2" onClick={fetchFromOrigin}>↓ Fetch</li>
                </ul>
            </div>
        </div>
    </div>
}

export default RepoControls