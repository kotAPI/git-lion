const NavTab = ({children,active,openTab,currentTab, tab})=>{
    let activeClasses =  tab.label===currentTab.label?'font-bold bg-white border-gray-800 text-black':'font-light border-white0 border-b'

    const handleTabClick = ()=>{
        openTab(tab)
    }


    return <div onClick={handleTabClick} className={`${activeClasses} border py-2 px-4 text-xs`}>{children}</div>
}

export default NavTab