import NavTab from "./NavTab/NavTab"

const NavBar = ({tabs=[],currentTab={},openTab}) => {

    //https://github.com/electron-react-boilerplate/electron-react-boilerplate/issues/1158#issuecomment-319433525

    return <div className="px-20 py-2 bg-gray-800 w-full navbar fixed top-0 l-0 z-50">
        <div className="items-center flex w-full">
            <div className="w-auto flex items-center mr-2">
                <img className="block mr-1" width={40} src="https://git-scm.com/images/logos/downloads/Git-Logo-White.png"></img>
                <span className="text-white font-bold text-sm block">Ember</span>
            </div>
            <div className="tabs-come-here w-1/2 pl-6 text-white">
                <ul className="flex">
                    {tabs.map((tab,idx)=>{
                        return <NavTab openTab={openTab} currentTab={currentTab}  tab={tab} key={idx}>{tab.label}</NavTab>
                    })}
                </ul>
            </div>
        </div>
    </div>
}

export default NavBar