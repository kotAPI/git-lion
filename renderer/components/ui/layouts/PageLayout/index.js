const PageLayout = ({children})=>{
    return <div className="overflow-y-hidden w-full" style={{marginTop:"50px",height:"calc(100vh - 30px)"}}>{children}</div>
}

export default PageLayout