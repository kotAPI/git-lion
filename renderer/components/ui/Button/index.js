const Button = ({children,onClick, ...props})=>{
   return  <button onClick={onClick} className="border border-black" {...props}>{children}</button>
}

export default Button