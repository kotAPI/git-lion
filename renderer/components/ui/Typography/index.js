const H1 = ({children})=>{
    return <h1 className="font-bold text-2xl text-blue-900">{children}</h1>
}


const Typography = {
    H1:H1
}

export default Typography