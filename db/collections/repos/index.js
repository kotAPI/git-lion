import _ from "lodash"
import DB from "../../index"
const Repos = {
    fetchRecent: async () => {
        try {
            let store  = DB.getStore()
            let repos =  store.get("recentRepos") || []
   
            return repos
        } catch (err) {
            console.log(err)
        }
    },
    deleteRecentRepo: async(repo)=>{
        let store = DB.getStore()
        let repos = store.get("recentRepos") || []
        let filteredRepos = _.filter(repos,(repoItem)=>{
            return repo.path !==  repoItem.path
        }) || []
        store.set("recentRepos",filteredRepos)
        return filteredRepos
    },
    addNewRepo: async (repo) => {
        try {
            let store  = DB.getStore()
            let repos = store.get("recentRepos") || []
            
            // TODO: check if repo already exists before pushing    
            repos.push(repo)
            store.set("recentRepos",repos)

        } catch (err) {
            console.log("Error adding repo",err)
        }
    }
}

export default Repos;