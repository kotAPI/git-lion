import Store from 'electron-store';

const APP_STORE_NAME = "git-log-app"

let store 
const initalize = ()=>{
    if(store){
        return store
    }
    // if it doesnt exist, create one
    store = new Store({ APP_STORE_NAME });
    return store;
}

const DB = {
   init:()=>{
    return initalize();
   },
   getStore:()=>{
    return initalize()
   }
}

export default DB